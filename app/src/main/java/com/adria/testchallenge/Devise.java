package com.adria.testchallenge;

public class Devise {

        private String name;
        private double rate;
        private String base;
        private String date;

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public Devise(String name, double rate) {
            this.name = name;
            this.rate = rate;
        }

        public String getName() {
            return name;
        }

        public double getRate() {
            return rate;
        }
    }
