package com.adria.testchallenge;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviseAdapter extends BaseAdapter {



    private Context context;
    private LayoutInflater layoutInflater;
    private List<Devise> deviseList;
    private DeviseItemClickListener deviseItemClickListener;

    public DeviseAdapter(Context context, List<Devise> deviseList, DeviseItemClickListener deviseItemClickListener) {
        this.context = context;
        this.deviseList = deviseList;
        this.deviseItemClickListener = deviseItemClickListener;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return deviseList.size();
    }

    @Override
    public Object getItem(int i) {
        return deviseList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View deviseItemView = layoutInflater.inflate(R.layout.devise_item, null);
        TextView tvName = (TextView) deviseItemView.findViewById(R.id.Name);
        TextView tvRate = (TextView) deviseItemView.findViewById(R.id.Rate);
        TextView date = (TextView)deviseItemView.findViewById(R.id.date);

        final Devise d = deviseList.get(i);
        tvName.setText(d.getName());
        tvRate.setText(Double.toString(d.getRate()));

        date.setText(d.getDate());

        deviseItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deviseItemClickListener.onDeviseItemClick(d);
            }
        });
        return deviseItemView;
    }
}


