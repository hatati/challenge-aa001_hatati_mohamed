package com.adria.testchallenge;

import android.content.Intent;
import android.os.Bundle;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<DeviseExchange>, DeviseItemClickListener {
    @BindView(R.id.base)
    TextView base;
    private ListView lvCurrency;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        lvCurrency = (ListView) findViewById(R.id.lvDevise);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadDeviseExchangeData();
    }

    private void loadDeviseExchangeData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.exchangeratesapi.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DeviseExchangeService service = retrofit.create(DeviseExchangeService.class);
        Call<DeviseExchange> call = service.loadDeviseExchange();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<DeviseExchange> call, Response<DeviseExchange> response) {

        DeviseExchange deviseExchange = response.body();
        base.setText(deviseExchange.getBase());
        lvCurrency.setAdapter(new DeviseAdapter(this, deviseExchange.getDeviseList(), this));

}

    @Override
    public void onFailure(Call<DeviseExchange> call, Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeviseItemClick(Devise d) {
        //Toast.makeText(this, c.getName(), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, GrapheActivity.class);
        intent.putExtra("name", d.getName());
        intent.putExtra("rate", d.getRate());

        startActivity(intent);
    }
}
